% qttinysa(1) | User Commands
%
% "February  5 2024"

# NAME

qttinysa - PC Interface for TinySA and TinySA Ultra Spectrum Analysers

# SYNOPSIS

**qttinysa** **no options available** 

# DESCRIPTION

This manual page documents briefly the **qttinysa** Spectrum Analyzer GUI.

This manual page was written for the Debian distribution because the
original program does not have a manual page. 

**qttinysa** is a program that attempts to replicate some of the TinySA 
Ultra and TinySA on-screen commands on the PC - except for the on-screen
commands related to generator control.

# OPTIONS

The program does not have any command line options.

# FILES

The program does not have any configuration files.

# ENVIRONMENT


# DIAGNOSTICS

The following diagnostics may be issued on stderr:

TinySA not found
:   A TinySA Device was not found.
Found TinySA on <device-id>
:   A TinySA Device was found
Serial Port Exception
:   There was a exception while communicating over the serial port.
Database file missing
:   The internal database file was not found.

**qttinysa** provides some return codes, that can be used in scripts:

    Code Diagnostic
    0 Program exited successfully.
    1 The configuration file seems to be broken.

# BUGS

The program is currently limited to only work with python3 and PyQT5.

The upstream BTS can be found at https://github.com/g4ixt/QtTinySA/issues.

# AUTHOR

Iaan Jefferson <todo@todo.net>
:   Author of QtTinySA

Patrick Winnertz <winnie@der-winnie.de>
:   Wrote this manpage for the Debian system.

# COPYRIGHT

Copyright © 2024 Patrick Winnertz

This manual page was written for the Debian system (and may be used by
others).

Permission is granted to copy, distribute and/or modify this document under
the terms of the GNU General Public License, Version 2 or (at your option)
any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.

